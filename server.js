const fs = require("fs");
const http = require('http');
const async = require("async");
const crypto = require("crypto");
const Docker = require("dockerode");
const byline = require("byline");
const StringDecoder = require("string_decoder").StringDecoder;
const decoder = new StringDecoder("utf8");

var server = http.createServer(function(request, response) {

  if (request.url === "/compiler") {
    sendFileContent(response, "index.html", "text/html");
  } else if (request.url === "/") {
    response.writeHead(200, {
      'Content-Type': 'text/html'
    });
    response.write('<b>Hey there!</b><br /><br />This is the default response. Requested URL is: ' + request.url);
  } else if (/^\/[a-zA-Z0-9\/]*.js$/.test(request.url.toString())) {
    sendFileContent(response, request.url.toString().substring(1), "text/javascript");
  } else if (/^\/[a-zA-Z0-9\/]*.css$/.test(request.url.toString())) {
    sendFileContent(response, request.url.toString().substring(1), "text/css");
  } else {
    console.log("Requested URL is: " + request.url);
    response.end();
  }
});

function sendFileContent(response, fileName, contentType) {
  fs.readFile(fileName, function(err, data) {
    if (err) {
      response.writeHead(404);
      response.write("Not Found!");
    } else {
      response.writeHead(200, {
        'Content-Type': contentType
      });
      response.write(data);
    }
    response.end();
  });
}

const io = require("socket.io")(server);
const port = 9000;

const docker = new Docker({
  socketPath: "/var/run/docker.sock"
});

io.on("connection", function(socket) {
  console.log("received connection");
  socket.on("code", function(code) {
    console.log(code);
    const uniqueId = crypto.randomBytes(16).toString("hex");
    fs.writeFileSync(`/tmp/${uniqueId}.chpl`, code, "utf8");

    docker.createContainer({
        Image: "prabhnith/chapel-1.14.0-image",
        name: uniqueId,
        Cmd: ['bash', '-c', `source /home/chapel-1.14.0/util/setchplenv.bash; \
                            cd /tmp/;\
                            chpl ${uniqueId}.chpl;./a.out;`],
        Tty: true,
        Binds: ["/tmp:/tmp"],
        Env: [`UNIQUE_ID=${uniqueId}`]
      },
      (err, container) => {
        console.log(err);
        container.attach({
            stream: true,
            stdout: true,
            stderr: true
          },
          (err, stream) => {
            console.log(err);
            byline(stream).on("data", line => {
              console.log("********BYLINE STREAM DATA***********");
              const decodedData = decoder.write(line);
              socket.emit("outputStream", {
                uniqueId,
                type: "data",
                stream: decodedData
              });
              console.log(decodedData);
            });

            byline(stream).on("error", line => {
              console.log("*************BYLINE STREAM ERROR**********");
              const decodedData = decoder.write(line);
              socket.emit("outputStream", {
                uniqueId,
                type: "error",
                stream: decodedData
              });
              console.log(decodedData);
            });

            stream.on("end", () => {
              console.log(" *********STREAM END**********");
              socket.emit("outputStream", {
                uniqueId,
                type: "exit"
              });
              container.stop(function(container) {
                console.log("******CONTAINER STOPPED********");
              });

              container.remove(function(err, data) {
                console.log(err, data);
              });
            });

            container.start((err, data) => {
              console.log("************CONTAINER STARTED**********");
              console.log(err, data);
            });
          }
        );
      }
    );
  });
});

server.listen(port);
console.log(`server listening on ${port}...`);