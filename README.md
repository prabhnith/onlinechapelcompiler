# README #

This is the repository for chapel Online Compiler hosted on amazon aws server.

### What is this repository for? ###

* Chapel is a new programming language and it is still in the development phase.
Many language have online compiler which make it easy to test the code online without installing any software on the local computer.
* This online compiler also helps you to get the output online and you will be able to run all the examples online before diving further in the language
* This compiler is in the development phase and all changes to the chapel compiler will also be reflected in the online compiler.


### How do I get set up? ###

* just visit http://ec2-52-10-105-242.us-west-2.compute.amazonaws.com:9000/compiler
* write the code in the programming area and hit compiler
* you will get the output in the output box

### Screenshot of the Chapel Compiler ###
![onlineChapelCompiler](https://bitbucket.org/prabhnith/onlinechapelcompiler/downloads/onlineChapelCompiler.jpg)


### Contribution guidelines ###

* just raise the issue 
* Give the pull request, we will review and merge it
* contributions are welcome because we love open source

### Who do I talk to? ###

* chapel IRC channel #chapel-developers