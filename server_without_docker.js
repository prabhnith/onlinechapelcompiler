var fs = require("fs");
var cmd = require('node-cmd');
var http = require('http');
var uuid = require('uuid4');
var WebSocketServer = require('websocket').server;
var port = 9000;

var server = http.createServer(function (request, response) {

	if (request.url === "/compiler") {
		sendFileContent(response, "index.html", "text/html");
	}
	else if (request.url === "/") {
		response.writeHead(200, { 'Content-Type': 'text/html' });
		response.write('<b>Hey there!</b><br /><br />This is the default response. Requested URL is: ' + request.url);
	}
	else if (/^\/[a-zA-Z0-9\/]*.js$/.test(request.url.toString())) {
		sendFileContent(response, request.url.toString().substring(1), "text/javascript");
	}
	else if (/^\/[a-zA-Z0-9\/]*.css$/.test(request.url.toString())) {
		sendFileContent(response, request.url.toString().substring(1), "text/css");
	}
	else {
		console.log("Requested URL is: " + request.url);
		response.end();
	}
}).listen(port);
console.log("SErver si listening on port ", port);
wsServer = new WebSocketServer({
	httpServer: server,
	port: 9000
});


//console.log(wsServer);
wsServer.on('request', function (request) {
	console.log("New WS request");
	var connection = request.accept(null, request.origin);

	// This is the most important callback for us, we'll handle
	// all messages from users here.
	connection.on('message', function (message) {
		console.log("New message received");
		if (message.type === 'utf8') {
			// process WebSocket message
			// console.log("message received", message);
			var inputCode = JSON.parse(message.utf8Data).code;
			console.log("code is :\n", inputCode);
			var initial = new Date().getTime();

			//create uuid file name
			var fileName = uuid() + ".chpl";

			fs.writeFile("./chpl_programs/" + fileName, inputCode, function (err) {
				if (err) {
					return console.log(err);
				}
				var aftersave = new Date().getTime();
				console.log("The file was saved! ", aftersave - initial);

				beforeCompile = new Date().getTime();

				var compileChpl = "chpl " + "./chpl_programs/" + fileName;
				cmd.get(
					compileChpl,
					function (data, err, stderr) {
						if (!err) {
							afterComp = new Date().getTime();
							console.log("After compilg time ", afterComp - beforeCompile);
							cmd.get('./a.out', function (data) {
								console.log('The output of the file is : ', data)
								connection.send(JSON.stringify({ "Output": data }));
								console.log("Output send to the client.");
							});
						} else {
							console.log('The error in file is : ', stderr)
							connection.send(JSON.stringify({ "Output": stderr }));
							console.log("Output send to the client.");
						}
					}
				);
				// fs.close(fd, function (err) {
				// 	if (err) {
				// 		console.log(err);
				// 	}
				// 	console.log("File closed successfully.");
				// });
			});
		}
	});
	connection.on('close', function (connection) {
		// close user connection
	});
});

function sendFileContent(response, fileName, contentType) {
	fs.readFile(fileName, function (err, data) {
		if (err) {
			response.writeHead(404);
			response.write("Not Found!");
		}
		else {
			response.writeHead(200, { 'Content-Type': contentType });
			response.write(data);
		}
		response.end();
	});
}